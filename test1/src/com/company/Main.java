package com.company;

public class Main {

    public  static void printStringArray(String[] arrayForPrint, boolean isWithLength) {
        System.out.print("contains values: ");
        for (int i = 0; i < arrayForPrint.length; i++) {
            if (arrayForPrint[i] != null) {
                System.out.print(arrayForPrint[i]);
                if (isWithLength) {
                    System.out.print("(" + arrayForPrint[i].length() + ") ");
                } else {
                    System.out.print(" ");
                }
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
	// write your code here
        String[] array1 = new String[args.length];
        int array1cur = 0;
        String[] array2 = new String[args.length];
        int array2cur = 0;
        String[] array3 = new String[args.length];
        int array3cur = 0;
        for (int i = 0; i < args.length; i++) {
            try
            {
                int xxx = Integer.parseInt(args[i].substring(0,3));
                if (xxx > 500) {
                    array1[array1cur] = args[i].toLowerCase();
                    array1cur ++;
                }
                else if (xxx < 500) {
                    array2[array2cur] = args[i].toLowerCase();
                    array2cur ++;
                }
                else {
                    String tmpStr = "";
                    for (int j = args[i].length() - 1; j >= 0; j--) {
                        tmpStr += args[i].charAt(j);
                    }
                    array3[array3cur] = tmpStr;
                    array3cur ++;
                }
            }
            catch (NumberFormatException e) {
                System.out.println("First three char in string " + (i + 1) + "is not number");
            }
        }
        System.out.print("Array1");
        printStringArray(array1, false);
        System.out.print("Array2 ");
        printStringArray(array2, false);
        System.out.print("Array3 ");
        printStringArray(array3, false);
    }
}
