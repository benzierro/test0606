package com.company;

public abstract class Shapes {
    protected double area;

    public abstract void calcArea();
    public abstract void showArea();
}
