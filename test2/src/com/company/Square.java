package com.company;

public class Square extends Shapes {
    private int a;

    public Square(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    @Override
    public void calcArea() {
        area = Math.pow(a, 2);
    }
    @Override
    public void showArea() {
        System.out.println("Area of sqare = " + area);
    }

}
