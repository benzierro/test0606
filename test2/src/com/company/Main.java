package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random rand = new Random();
        int count = rand.nextInt(3) + 2;
        System.out.println("Please enter information about " + count + " figures.");
        Shapes[] shapes = new Shapes[count];
        for (int j = 0; j < count; j++) {
            boolean fl = true;
            while (fl) {
                System.out.print("Please enter information about " + (j + 1) + " figures (one field for quare - a; two field triangle - a h;): ");
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                    String readValue = reader.readLine();
                    String[] str = readValue.split(" ");
                    if (str.length == 1)
                    {
                        shapes[j] = new Square(Integer.parseInt(str[0]));
                        fl = false;
                    }
                    else if (str.length == 2) {
                        shapes[j] = new Triangle(Integer.parseInt(str[0]), Integer.parseInt(str[1]));
                        fl = false;
                    }
                    else {
                        System.out.print("Input string containts not one or two fields. ");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                catch (NumberFormatException e) {
                    System.out.print("First and second fields must be number. ");
                }
            }
        }
        for (int i = 0; i < shapes.length; i++) {
            shapes[i].calcArea();
            shapes[i].showArea();
        }

    }
}
