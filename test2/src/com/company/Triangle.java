package com.company;

public class Triangle extends Shapes {
    private int a;
    private int h;

    public Triangle(int a, int h) {
        this.a = a;
        this.h = h;
    }
    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    @Override
    public void calcArea() {
        area = a * h * 0.5;
    }
    @Override
    public void showArea() {
        System.out.println("Area of triangle = " + area);
    }
}
